require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Depot
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.generators do |g|
      g.test_framework :rspec, #telling to use test_framework as rspec 
        fixtures: true, # specifies to generate a fixture for each model using factoryGirl
        view_specs: false, # skips generating view
        helper_specs: false, #skips generating specs for the helper files.
        routing_specs: false,
        controller_specs: true,
        request_specs: false#skips rspec's defaults for adding integration-level specs
      g.fixture_replacement :factory_bot, dir: "spec/factories"  

    end
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
