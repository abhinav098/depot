Rails.application.routes.draw do

  get 'pictures/get'
  post 'pictures/save'
  get 'pictures/show'
  get 'pictures/picture'

  resources :pictures
  get 'admin', to: 'admin#index'

  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  resources :users


  resources :products do
    get :who_bought, on: :member
  end

  scope '(:locale)' do
    resources :orders
    resources :line_items
    resources :carts
    root to: 'store#index'
  end
  get 'store/index'
  get 'contact', to: 'store#contact', as: :contact
end
