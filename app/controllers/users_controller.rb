class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  
  def index
    @users  = User.order(:name)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user
      redirect_to users_url, notice: "User #{@user.name} was successfully created."
    else
      render 'new', notice: @user.errors
    end
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to @user, notice: "User was successfully updated."
    else
      render 'edit'
    end
  end

  def destroy
    if @user.destroy
      redirect_to users_url
    end
  end
  rescue_from 'User::Error' do |exception|
    redirect_to users_url, notice: exception.message
  end 

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation)
  end
end
