class OrdersController < ApplicationController
  include CurrentCart
  skip_before_action :authorize, only: [:new, :create]
  before_action :set_cart, only: [:new, :create]
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :check_cart, only: :new

  def new
    @order = Order.new
  end

  def index
    @orders = Order.order('created_at desc').page(params[:page]).per(30)
  end

  def create
    @order = Order.new(order_params)
    @order.add_line_items_from_cart(@cart)
    if @order.save
      Cart.destroy(session[:cart_id])
      OrderMailer.received(@order).deliver_later
      session[:cart_id] = nil
      redirect_to store_index_url, notice: 'Thank you for your order.'
    else
      render :new
    end
  end

  def update
    if @order.update(order_params)
      redirect_to @order, notice: 'Order was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @order.destroy
    redirect_to store_index_url, notice: 'Order was successfully destroyed.'
  end

  private

    def check_cart
      if @cart.line_items.empty?
        redirect_to store_index_url, notice: 'Your Cart was empty'
      end
    end

    def set_order
      @order = Order.find(params[:id])
    end

    def order_params
      params.require(:order).permit(:name, :address, :email, :pay_type)
    end
end
