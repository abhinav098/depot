class CartsController < ApplicationController
  skip_before_action :authorize, only: [:create, :update, :destroy]
  include CurrentCart
  before_action :set_cart, only: [:destroy]
  def index
    @carts = Cart.all 
  end

  def show
    begin
      @cart = Cart.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error "Attempt to access invalid cart #{params[:id]}"
      redirect_to store_path, notice: "Invalid cart"  
    end 
  end

  def destroy
    @cart.destroy
    session[:cart_id] = nil
    redirect_to store_index_url
  end
end
