class ProductsController < ApplicationController
  before_action :find_product, only: [:show,:edit, :update, :destroy]

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    respond_to do |f|
      if @product.save
        f.html { redirect_to @product }
        f.js 
      else
        f.html { render 'new' }
      end
    end
  end
  
  def index
    @products = Product.all
  end

  def update
    if @product.update(product_params)
      redirect_to @product, notice: "Product Updated"
      @products = Product.all
      ActionCable.server.broadcast 'products', html: render_to_string('store/index',layout: false)
    else
      render 'edit'
    end
  end

  def who_bought
    @product = Product.find(params[:id])
    @latest_order = @product.orders.order(:updated_at).last
    if stale?(@latest_order)
      respond_to do |format|
        format.atom
      end
    end
  end

  def destroy
    if @product.destroy
      redirect_to products_path, notice: "Product Deleted"
    end
  end

  private
  def product_params
    params.require(:product).permit(:title, :description, :image_url, :price)
  end
  def find_product
    @product = Product.find(params[:id]) 
  end
end

