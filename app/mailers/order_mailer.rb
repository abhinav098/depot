class OrderMailer < ApplicationMailer
  
  def received(order)
    @order = order

    mail to: order.email, subject: "Abby Bookshelf Store Order Confirmation"
  end

  def shipped(order)
    @order = order
    mail to: order.email, subject: 'Abby Bookshelf Store Order Shipped'
  end
end
