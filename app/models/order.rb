class Order < ApplicationRecord
  validates :name, :email, :address, :pay_type, presence: true

  has_many :line_items, dependent: :destroy

  TYPES = ["Check", "Credit card", "Purchase order"].freeze

  enum pay_type: TYPES

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end
end
