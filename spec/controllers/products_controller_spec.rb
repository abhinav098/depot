require 'rails_helper'
include SpecTestHelper

describe ProductsController do
  
  describe 'GET #index #new #edit #show' do
  
    context "when not logged in" do
      it "should redirect to login" do
        get :index
        expect(response).to redirect_to login_url
      end
    end  
  
    context "when logged_in" do
      before do
        login_user
      end
    
      it "should get index" do
        x = create(:product)
        y = create(:product)
        get :index
        expect(assigns :products).to match_array([x, y])
        expect(response).to render_template("index")
      end
      
      it "should assign instance variable and get new" do
        get :new
        expect(assigns :product).to be_a_new(Product)
        expect(response).to render_template("new")
      end

      it "should get edit" do
        edit_product = create(:product)
        get :edit, params:{ id: edit_product }
        expect(assigns :product).to eq edit_product
        expect(response).to render_template("edit")
      end
      
      it "should get show" do
        product = create(:product)
        get :show, params:{id: product}
        expect(assigns :product).to eq product
        expect(response).to render_template("show")
      end
    end
  end

  describe "post create " do
    context "valid attributes" do
      before do
        login_user
      end
      it "should create product and increase product count" do
        expect{ post :create, params: {product: attributes_for(:product)} }.to change(Product, :count).by(1)
      end
      it "should redirect_to show post create" do
        post :create, params:{product: attributes_for(:product)}
        expect(response).to redirect_to product_path(assigns[:product])
      end
    end
  end
  context "invalid attributes" do
    before do
      login_user
    end
    it "should not create product" do
      post :create, params: {product: attributes_for(:product, title: nil)}
      expect(response).to render_template("new")     
    end
  end

  describe "update test" do
    context "valid attributes" do
      before do
        login_user
      end
      it "should update the product" do
        @product = create(:product, title:"test update")
        patch :update, params:{id: @product, product: attributes_for(:product)}
        expect(assigns :product).to eq(@product)
      end
      it "should redirect_to updated product" do
        @product = create(:product, title:"test update1")
        patch :update, params:{id: @product, product: attributes_for(:product)}
        expect(response).to redirect_to @product  
      end
    end
    context "invalid valid attributes" do
      before do
        login_user
      end
      it "should not update the product" do
        @product = create(:product, title:"test update")
        patch :update, params:{id: @product, product: attributes_for(:product, title:"test update2", description:nil)}
        @product.reload
        expect(@product.title).to_not eq("test update2")
      end
      it "should not redirect_to updated product" do
        @product = create(:product, title:"test update1")
        patch :update, params:{id: @product, product: attributes_for(:product, description: nil)}
        expect(response).to render_template "edit"  
      end
    end
  end 
  describe "destroy"do 
    before :each do
      login_user
      @product = create(:product)
    end
    it "should destroy the product" do
      expect{delete :destroy, params: {id: @product}}.to change(Product, :count).by(-1)
    end
    it "should redirect to " do
      delete :destroy, params: {id: @product}
      expect(response).to redirect_to products_url
    end
  end
end


