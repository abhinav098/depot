module Macros
  def sign_in(user)
    visit root_path
    click_button 'Login'
    fill_in 'Name', with: user.name
    find('input[type="password"]').set user.password
    click_button 'Log In'
  end
end