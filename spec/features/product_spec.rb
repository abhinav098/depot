require 'rails_helper'
include Macros

feature 'UserManagement' do 
  scenario 'adds a new product' do
    user = create(:user)
    sign_in(user)

    visit root_path
    expect{
      click_link 'Products'
      click_link 'here'
      fill_in 'Title', with: "title 1"
      fill_in 'Description', with: 'hello123'
      fill_in 'Price', with: 10
      fill_in 'Image url', with: 'lorem.png'
      click_button 'Create Product'
    }.to change(Product, :count).by(1) 
  end
end

