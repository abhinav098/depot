require 'rails_helper'
include Macros

feature 'UserManagement' do 
  scenario 'adds a new user' do
    user = create(:user)
    sign_in(user)

    visit root_path
    expect{
      click_link 'Users'
      click_link 'New User'
      fill_in 'Name', with: "new user1"
      find('#password').set 'hello123'
      find('#password_confirmation').set 'hello123'
      click_button 'Create User'
    }.to change(User, :count).by(1) 
    expect(current_path).to eq users_path
    within 'h1' do
      expect(page).to have_content 'Users'      
    end
  end
end