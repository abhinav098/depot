require 'rails_helper'

describe Product do
  it "is a valid factory" do
    expect(build(:product)).to be_valid
  end
  
  it "is invalid with duplicate title" do
    create(:product, title: "hum tumhare hai sanam")
    p1 = build(:product, title: "hum tumhare hai sanam")
    expect(p1).to have(1).errors_on(:title)
  end

  it "is valid with title, description, price and image_url" do
    p2 = build(:product)
    expect(p2).to be_valid
  end

  it "is invalid without title" do
    product = build(:product, title: nil)
    expect(product).to have(1).errors_on(:title)
  end

  it "is invalid without description" do
    product = build(:product, description: nil)
    expect(product).to have(1).errors_on(:description)
  end

  it "is invalid without price" do
    product = build(:product, price: nil)
    expect(product).to have(2).errors_on(:price)
  end

  it "is invalid without image_url" do
    product = build(:product, image_url: nil)
    expect(product).to have(1).errors_on(:image_url)
  end

  it "should match the title name" do
    product = build(:product)
    expect(product.book_name).to eq product.title
  end

  it "is invalid for price less than 0.1" do
    product = build(:product, price: 0.0)
    expect(product).to have(1).errors_on(:price)
  end

  it "is valid for price greater than 0.1" do
    product = build(:product, price: 1)
    expect(product).to be_valid
  end

  it "has number of line_items" do
    product = create(:product)
    expect(product.line_items.count).to eq 2
  end
end