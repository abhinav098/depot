FactoryBot.define do
  factory :line_item do
    association :order
    association :cart
    quantity 1
  end
end