require 'faker'

FactoryBot.define do
  factory :user do
    name { Faker::Name.name}
    password "hello123"
    password_confirmation "hello123"
  end
end