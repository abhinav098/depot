require 'faker'
FactoryBot.define do
  factory :product do
    title { Faker::Book.title}
    description "Test Price"
    price 10
    image_url "lorem.png"

    after(:create) do |product|
      2.times do 
        product.line_items << FactoryBot.build(:line_item)
      end
    end
  end

  factory :product1 do
    title nil
    description nil
    price 10
    image_url nil
  end
end